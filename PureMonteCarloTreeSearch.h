#include "Node.h"
#include "Board.h"
#include "State.h"
#include <ctime>
#include <iostream>

Node GenerateRandomChild(Node node) {
    int possibleMoves = node.state.allPossibleMovesByTurnPlayer.size;
    if (possibleMoves == 0) {
        node.state.switch_turnPlayer();
        possibleMoves = node.state.allPossibleMovesByTurnPlayer.size;
    }
    if (possibleMoves == 0) {
        return node;
    }
    int position = rand() % possibleMoves;
    State newChildState = State(node.state.get_new_board(node.state.allPossibleMovesByTurnPlayer.get_vec_data(position)),
            node.state.otherPlayer, node.state.turnPlayer);
    newChildState.set_lastMove(node.state.allPossibleMovesByTurnPlayer.get_vec_data(position));
    Node newChildNode = Node(newChildState, &node);
    return newChildNode;
}

int simulate(Node* node) {
    int status = node->state.check_status();
    Node* playNode = node;
    Node randomNode;
    while (status == CONTINUE) {
        randomNode = GenerateRandomChild(*playNode);
        status = randomNode.state.check_status();
        playNode = &randomNode;
    }
    return status;
}

void update_score(Node* node, int result, int callingPlayer) {
    if (result == callingPlayer) {
        node->increment_wins();
    }
    else if (result == DRAW) {
        node->increment_draws();
    }
    else{
        node->increment_losses();
    }
}

DoublePointer PureMonteCarloTreeSearch(Node* node, int callingPlayer) {
    node->expand_node();
    if (node->children.size == 0) {
        return nullptr;
    }

    int result, index = 0, playouts = 0;
    time_t startTime = time(nullptr), elapsedTime = 0;
    while (elapsedTime <= 5) {
        if (index == node->children.size) {
            index = 0;
        }

        result = simulate(&node->children.vec[index]);
        update_score(&node->children.vec[index], result, callingPlayer);

        elapsedTime = time(nullptr) - startTime;
        index++;
        playouts++;
    }

    cout << "Playouts per second: " << playouts / elapsedTime << "s" << endl;
    auto resultNode = node->get_best_child();
    return resultNode->state.board.get_board_array();
}

void update_heuristics(Node* node) {
    node->increment_corner();
	node->increment_corner_adjacent();
    node->increment_frontier();
    node->increment_stability();
}

DoublePointer PureMonteCarloTreeSearchHeuristics(Node* node, int callingPlayer) {
    node->expand_node();
    if (node->children.size == 0) {
        return nullptr;
    }
    int playouts = 0;

    int result, index = 0;
    time_t startTime = time(nullptr), elapsedTime = 0;
    while (elapsedTime <= 5) {
        if (index == node->children.size) {
            index = 0;
        }

        update_heuristics(&node->children.vec[index]);
        result = simulate(&node->children.vec[index]);
        update_score(&node->children.vec[index], result, callingPlayer);

        elapsedTime = time(nullptr) - startTime;
        index++;
        playouts++;
    }

    cout << "Playouts per second: " << playouts / elapsedTime << "s" << endl;
    auto resultNode = node->get_best_heuristic_child();

    if (resultNode == nullptr) {
        return nullptr;
    }

    return resultNode->state.board.get_board_array();
}