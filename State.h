#ifndef STATE_H
#define STATE_H

#include "Board.h"

#define CONTINUE -1
#define DRAW 0

class State {
public:
    Board board = Board();
    int turnPlayer;
    int otherPlayer;
	Index lastMove = Index();
    VectorWrapper<Index> allPossibleMovesByTurnPlayer = VectorWrapper<Index>(0);

    State();
    State(const Board& board, int turnPlayer, int otherPlayer);
    State(const State &_state);
    Board get_new_board(Index position);
    void set_lastMove(Index _lastMove);
    int check_status();
    bool no_possible_moves();
    void update_state(int newBoardArray[BoardSize][BoardSize]);
    void switch_turnPlayer();
    VectorWrapper<State> get_possible_states();
};

#endif //STATE_H
