#include "State.h"
#include <vector>
using namespace std;

State::State() {}

State::State(const Board& _board, int _turnPlayer, int _otherPlayer) {
    board = _board;
    turnPlayer = _turnPlayer;
    otherPlayer = _otherPlayer;
    allPossibleMovesByTurnPlayer = board.get_legal_positions(turnPlayer);
}

State::State(const State &_state) {
    board = _state.board;
    turnPlayer = _state.turnPlayer;
    otherPlayer = _state.otherPlayer;
    allPossibleMovesByTurnPlayer = _state.allPossibleMovesByTurnPlayer;
    lastMove.x = _state.lastMove.x;
    lastMove.y = _state.lastMove.y;
}

VectorWrapper<State> State::get_possible_states() {
    if (no_possible_moves()) return VectorWrapper<State>(0);
	
    vector<State> vec(allPossibleMovesByTurnPlayer.size);
    for (int i = 0; i < allPossibleMovesByTurnPlayer.size; i++) {
        // Children of the current state spawned from moves made by the turn player
        // The players are then switched in the new state
        vec[i] = State(get_new_board(allPossibleMovesByTurnPlayer.get_vec_data(i)), otherPlayer, turnPlayer);
        vec[i].set_lastMove(allPossibleMovesByTurnPlayer.get_vec_data(i));
    }
    VectorWrapper<State> allPossibleStates(vec, vec.size());
    return allPossibleStates;
}

Board State::get_new_board(Index position) {
    int x = position.x, y = position.y, row = 0, col = 0, enemies = 0;
    int (*boardArray)[BoardSize] = board.get_board_array();
    int newBoardArray[BoardSize][BoardSize];

    for (int i = 0; i < BoardSize; i++) {
        for (int j = 0; j < BoardSize; j++) {
            newBoardArray[i][j] = boardArray[i][j];
        }
    }

    for (row = x - 1; row >= 0; row--) {
        if (newBoardArray[row][y] == otherPlayer) {
            enemies++;
        }
        else {
            bool north = newBoardArray[row][y] == turnPlayer && enemies;
            if (north) {
                for (row = x - 1; enemies > 0; row--) {
                    newBoardArray[row][y] = turnPlayer;
                    enemies--;
                }
            }
            break;
        }
    }

    enemies = 0;
    for (row = x + 1; row < BoardSize; row++) {
        if (newBoardArray[row][y] == otherPlayer) {
            enemies++;
        }
        else {
            bool south = newBoardArray[row][y] == turnPlayer && enemies;
            if (south) {
                for (row = x + 1; enemies > 0; row++) {
                    newBoardArray[row][y] = turnPlayer;
                    enemies--;
                }
            }
            break;        }
    }

    enemies = 0;
    for (col = y - 1; col >= 0; col--) {
        if (newBoardArray[x][col] == otherPlayer) {
            enemies++;
        }
        else {
            bool west = newBoardArray[x][col] == turnPlayer && enemies;
            if (west) {
                for (col = y - 1; enemies > 0; col--) {
                    newBoardArray[x][col] = turnPlayer;
                    enemies--;
                }
            }
            break;
        }
    }

    enemies = 0;
    for (col = y + 1; col < BoardSize; col++) {
        if (newBoardArray[x][col] == otherPlayer) {
            enemies++;
        }
        else {
            bool east = newBoardArray[x][col] == turnPlayer && enemies;
            if (east) {
                for (col = y + 1; enemies > 0; col++) {
                    newBoardArray[x][col] = turnPlayer;
                    enemies--;
                }
            }
            break;
        }
    }

    enemies = 0;
    row = x - 1, col = y + 1;
    while (row >= 0 && col < BoardSize) {
        if (newBoardArray[row][col] == otherPlayer) {
            enemies++;
        }
        else {
            bool northeast = newBoardArray[row][col] == turnPlayer && enemies;
            if (northeast) {
                row = x - 1, col = y + 1;
                do {
                    newBoardArray[row][col] = turnPlayer;
                    enemies--;
                    row--;
                    col++;
                } while (enemies > 0);
            }
            break;
        }
        row--;
        col++;
    }

    enemies = 0;
    row = x + 1, col = y - 1;
    while (col >= 0 && row < BoardSize) {
        if (newBoardArray[row][col] == otherPlayer) {
            enemies++;
        }
        else {
            bool southwest = newBoardArray[row][col] == turnPlayer && enemies;
            if (southwest) {
                row = x + 1, col = y - 1;
                do {
                    newBoardArray[row][col] = turnPlayer;
                    enemies--;
                    col--;
                    row++;
                } while (enemies > 0);
            }
            break;
        }
        col--;
        row++;
    }

    enemies = 0;
    row = x - 1, col = y - 1;
    while (col >= 0 && row >= 0) {
        if (newBoardArray[row][col] == otherPlayer) {
            enemies++;
        }
        else {
            bool northwest = newBoardArray[row][col] == turnPlayer && enemies;
            if (northwest) {
                row = x - 1, col = y - 1;
                do {
                    newBoardArray[row][col] = turnPlayer;
                    enemies--;
                    row--;
                    col--;
                } while (enemies > 0);
            }
            break;
        }
        row--;
        col--;
    }

    enemies = 0;
    row = x + 1, col = y + 1;
    while (col < BoardSize && row < BoardSize) {
        if (newBoardArray[row][col] == otherPlayer) {
            enemies++;
        }
        else {
            bool southeast = newBoardArray[row][col] == turnPlayer && enemies;
            if (southeast) {
                row = x + 1, col = y + 1;
                do {
                    newBoardArray[row][col] = turnPlayer;
                    enemies--;
                    row++;
                    col++;
                } while (enemies > 0);
            }
            break;
        }
        row++;
        col++;
    }

    newBoardArray[x][y] = turnPlayer;

    return Board(newBoardArray);
}

void State::set_lastMove(Index _lastMove) {
    lastMove.x = _lastMove.x;
    lastMove.y = _lastMove.y;
}


int State::check_status() {
    // Return -1 -> game can continue
    // Return 0 -> game is a draw
    // Return 1 OR 2 -> player who won the game
    if (allPossibleMovesByTurnPlayer.size > 0) return CONTINUE;

    VectorWrapper<Index> movesByOtherPlayer = board.get_legal_positions(otherPlayer);
    if (movesByOtherPlayer.size > 0) return CONTINUE;

    bool isDraw = board.check_draw();
    if (isDraw) return DRAW;

    return board.check_win();
}

void State::update_state(int newBoardArray[BoardSize][BoardSize]) {
    board.update_board(newBoardArray);
    switch_turnPlayer();
}

bool State::no_possible_moves() {
    return allPossibleMovesByTurnPlayer.size == 0;
}

void State::switch_turnPlayer() {
    int temp = turnPlayer;
    turnPlayer = otherPlayer;
    otherPlayer = temp;
    allPossibleMovesByTurnPlayer = board.get_legal_positions(turnPlayer);
}
